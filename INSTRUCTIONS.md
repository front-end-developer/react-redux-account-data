Run the app with
npm start

I also created a backend but I never had time to plug it into the app.

The backend from my personal site:
GET BANK BALANCE:
	GET
		http://www.wisdomor.co.uk/exercises_1/account/index.php?balance=information
	

SEND MONEY TRANSFER:
	POST
	http://www.wisdomor.co.uk/exercises_1/moneytransfer/index.php
	POST:
		name: 'Mark'
		email: ll@lll.com
		amount: 10
		
	success returns:
		{"result":"success","uuid":"14"}
		
	failor returns:
		{'error': 'db not updated'}
	
	
GET ALL MONIES TRANSFERED:
	GET
	http://www.wisdomor.co.uk/exercises_1/moneytransfer/index.php?accounts=information
	POST:
	accounts: 'information'
	
	success returns:
		[
			{
				name: "Mark Webley",
				email: "test@wisdomor.co.uk",
				amount: "11000",
				date: null
			},
			{
				name: "Mw",
				email: "ll@ll.com",
				amount: "123",
				date: "2019-05-30 16:22:52"
			}
		]
		
__________________________
NOTES:
What you liked and disliked about this test? 
Which tasks were easy for you and which ones were difficult?
How long did it take you to complete it?
Please give us any feedback about what you have done, what you weren't able to do and how it went in general.


What I liked:
- the My account section, because I am already bulding an ecommerce solution, in react so I can kill two birds with one stone, (instead of building something that I then throw away).

- that json server api thing was interesting, thanks :)

Observations:
- I focused more on the UI, architectural, and data side of things.
- I did not do some typescript declarations because we have type inferance, and to save time for this test.

- I used sass because it works across many framework architectures, and I its structure is efficient.

- my approach to validation, I did not take an easy route, I wanted something more dynamic that works with errors, I did not want to depend on the input controls of minLenght, required etc, I wanted to do something different. I spend most time on - validateForm because I wanted to get it to work with any form component (with in reason of the time, and it can be extended with the common/models/ form code).

- I used boostrap instead of learning another framework again for a test, because in my owntime i am also looking at material another framework. I stick to learning things that have a long industrial shelf life.

Normally I do:
- container and presentational components,
- set up app architecture with more details,
- use custom architecture outside of create-react-app, create-react-app is just for basic setup.
- use observables.subscribe or async await promises for services
- partial components like styled input containers etc, I started it, but never finished it.

What I did & did not do:
- i used bootstrap instead of the framework you mentioned, because I have other tests and interviews.
- I used sass instead because it is industry standard and fits in pertfectly with different app architectures.
- I created a database and test api webservice, but never pluged it into the app because yet, because of time etc.

Dislike:
- coding up stuff I will through away, because the most important thing is time. Because when I am between contracts I code stuff that i can use, to generate income.

Thanks

Mark Webley | mrkwebley@gmail.com