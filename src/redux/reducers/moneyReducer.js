/**
 * Created by Mark Webley on 29/05/2019.
 */
import * as types from '../actions/allActionTypes';
export default function moneyReducer(state = [], action) {
    let newState;
    switch (action.type) {
            case types.SEND_MONEY:
            newState = ({...state,...action.money}); // [...state, {...action.money}]; // ({...state,...action.money});
            break;
        default:
            return state;
    }
    return newState;
}
