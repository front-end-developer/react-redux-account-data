/**
 * Created by Mark Webley on 29/05/2019.
 */
import * as types from '../actions/allActionTypes';
export default function accountReducer(state = [], action) {
    let newState;
    switch (action.type) {
        case types.CREATE_ACCOUNT_ITEM:
            newState =  [...state, {...action.account}]; // [{...state,...action.account}]; // ({...state,...action.account});
            break;
        case types.SAVE_ACCOUNT_ITEM:
            newState = ({...state,...action.account});
            break;
        default:
            return state;
    }
    return newState;
}