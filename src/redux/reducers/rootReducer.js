/**
 * Created by Mark Webley on 29/05/2019.
 */
import {combineReducers} from 'redux';
import account from './accountReducer';
import money from './moneyReducer';

const rootReducer = combineReducers({
    money,
    account
});

export default rootReducer;