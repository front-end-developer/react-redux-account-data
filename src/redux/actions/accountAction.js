/**
 * Created by Mark Webley on 29/05/2019.
 */
import * as types from './allActionTypes';
export function accountActionCreator(account) {
    return {
        type: types.CREATE_ACCOUNT_ITEM,
        account
    };
}

export function saveAccountActionCreator(account) {
    return {
        type: types.SAVE_ACCOUNT_ITEM,
        account
    };
}