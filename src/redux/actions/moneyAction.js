/**
 * Created by Mark Webley on 29/05/2019.
 */
import * as types from './allActionTypes';

export function sendMoneyToAccountActionCreator(money) {
    return {
        type: types.SEND_MONEY,
        money
    };
}
