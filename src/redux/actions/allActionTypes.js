/**
 * Created by Mark Webley on 29/05/2019.
 */
export const CREATE_ACCOUNT_ITEM = 'CREATE_ACCOUNT_ITEM';
export const SAVE_ACCOUNT_ITEM = 'SAVE_ACCOUNT_ITEM';
export const SEND_MONEY = 'SEND_MONEY';

