import React from 'react';
import ReactDOM from 'react-dom';
import {Provider as ReduxProvider} from 'react-redux';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import configureStore from './redux/configureStore';
import BankAccountService from "./modules/service/BankAccount.service";

//TODO: should/could update the store
const userBankInformation = BankAccountService.getMyAccount().subscribe({
    next: result => console.log('observable get bank account information: ', result),
    complete: () => console.log('done')
});;

const store = configureStore(); // <-- userBankInformation TODO... set bank info at start up
ReactDOM.render((
    <ReduxProvider store={store}>
        <App />
    </ReduxProvider>
), document.getElementById('root'));

serviceWorker.unregister();
