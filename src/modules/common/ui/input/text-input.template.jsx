/**
 * Created by Mark Webley on 31/05/2019.
 */
import React from 'react';
export const TextInputTemplate = (obj) => {
        return (
            <>
                <label htmlFor={obj.fieldId}>{obj.name}</label>
                <input onChange={obj.inputHandler}
                       className="col-12"
                       type="text"
                       name={obj.fieldId}
                       id={obj.fieldId}
                       value={obj.value} />
                <span className = "help-block {obj.formErrors}" > {obj.formErrorsString}</span>
            </>
        )
};