/**
 * Created by Mark Webley on 29/05/2019.
 */
export class SendMoneyFormErrors {
    formErrors = {
        'name': '',
        'email': '',
        'amount': ''
    }
}
