/**
 * Created by Mark Webley on 29/05/2019.
 */
export class ValidateSendMoney {
    messages = {
        'name': {
            'type': 'text',
            'required': 'Full name is required',
            'minlength': 'Please enter your full name',
        },
        'email': {
            'type': 'email',
            'required': 'Email is required',
            'minlength': 'Please enter a valid email',
        },
        'amount': {
            'type': 'number',
            'required': 'Amount is required',
            'minlength': 'Please enter a valid amount number',
        }
    }
}