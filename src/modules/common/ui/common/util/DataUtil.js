/**
 * Created by Mark Webley on 29/05/2019.
 */

// TODO: methods as static
export class DataUtil {

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    validateText(text) {
        return (text.length > 0);
    }

    validateDigits(digit) {
        return (digit.length > 0 && !isNaN(digit.toString()));
    }
}