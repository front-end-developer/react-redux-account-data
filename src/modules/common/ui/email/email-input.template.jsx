/**
 * Created by Mark Webley on 31/05/2019.
 */
import React from 'react';
export const EmailInputTemplate = (obj) => (
    <>
        <label htmlFor={obj.fieldId}>{obj.name}</label>
        <input onChange={obj.inputHandler}
               className="col-12"
               type="email"
               name={obj.fieldId}
               id={obj.fieldId}
               value={obj.value}/>
        <span className="help-block {obj.formErrors}">{obj.formErrorsString}</span>
    </>
);