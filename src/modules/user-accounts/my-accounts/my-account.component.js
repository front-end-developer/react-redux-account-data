/**
 * Created by Mark Webley on 29/05/2019.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import './my-account.component.scss';
import * as $ from 'jquery';
import 'jquery-knob';
import MoneyTransferService from "../../service/MoneyTransfer.service";

                                //  Personal Note: could do as a PureComponent but left it as is
class MyAccountComponent extends Component {

    state = {
        account: this.props.account || {},
        money: this.props.money || {
            startingBalance: 0,
            balance: 100,
            withdrawal: 0
        },
        knob: {
            value: 7,
            min: 0,
            max: 100,
            width: 100,
            height: 100,
            readOnly: 'readOnly',
            thickness: "0.5",
            displayInput: true,
            fgColor: '#ffb428',
            bgColor: '#eaeaee'
        }
    }

    getObservableService() {
        // TODO: finish testing - Backend service
        MoneyTransferService.getAccounts().subscribe({
            next: result => console.log('observable getMoneyTransfers results: ', result),
            complete: () => console.log('done')
        })
    }

    componentDidMount() {
        // TODO: finish testing - Backend API service, move to when we first load app pass into store
        this.getObservableService();

        const options = this.state.knob;
        $(".dial").knob({
            options,
            'format' : function (value) {
                return `${value}%`;
            }
        });

        this.setState((prevState, props) => {
            return {
                money: {
                    ...prevState.money,
                    startingBalance: props.money.balance
                }
            };
        });
    }

    setPercentage() {
        let value = parseInt((this.props.money.balance / this.state.money.startingBalance) * 100);
        if (!isNaN(value)) {
            this.setState((prevState, props) => ({
                knob: {
                    ...prevState.knob,
                    value: `${value}%`
                }
            }));
        }

        // Hack: because react state did not change the dial
        setTimeout(() => {
            $('.dial').trigger('change');
        }, 500);
    }

    /**
     * Personal Note: could use PureComponent instead
     * @param prevProps
     * @param prevState
     * @param snapshot
     */
    componentDidUpdate(prevProps, prevState, snapshot) {
       if (this.props.account !== prevProps.account) {
            this.setState((state, props) => {
                const newState = state.account.concat(props.account)
                return { account: newState};
            });
        }

        if (this.props.money !== prevProps.money) {
            this.setState((componentPrevState, props) => {
                const money = {
                    ...componentPrevState.money,
                    ...props.money
                };

                return {
                    money
                };
            });

            setTimeout(() => {
                this.setPercentage();
            }, 500);
        }
    }


    render() {
        const title = () => {
            return (
                <h1>My account</h1>
            )
        }

        const chartElements = () => {
            // TODO: was not working in my IDE
            // [value, fgColor, min, max] = {...this.state.knob};
            return (
                <section id="graphic-chart" className="container mb-3">
                    <div className="money-sent">
                        £{this.props.money.withdrawal || 0}
                        <span className="account-label">total sent</span>
                    </div>
                    <input type="text" className="dial"
                           value={this.state.knob.value}
                           data-thickness={this.state.knob.thickness}
                           data-readonly={this.state.knob.readOnly}
                           data-displayinput={this.state.knob.displayInput}
                           data-width={this.state.knob.width}
                           data-height={this.state.knob.height}
                           data-fgcolor={this.state.knob.fgColor}
                           data-min={this.state.knob.min} data-max={this.state.max} />
                    <div className="balance">
                        £{this.props.money.balance || 0}
                        <span className="account-label">left available</span>
                    </div>
                </section>
            )

        }

        const domElements = () => {

            if (this.props.account.length == 0 || (this.props.account.length == 1 && typeof this.props.account.amount === 'undefined')) {
                return (<></>);
            } else {
                return (
                    <>
                        <h3>Transactions</h3>
                        <table className="table">
                            <tbody>
                            {
                                this.props.account.map((beneficiary, key) => {
                                    if (beneficiary.name === '' || beneficiary.email === '' || beneficiary.amount === '') {
                                        return;
                                    }
                                    return (<tr key={key}>
                                        <td className="beneficiary">
                                            {beneficiary.name}
                                            <span>{beneficiary.email}</span>
                                        </td>
                                        <td className="amount">£{beneficiary.amount}</td>
                                    </tr>);
                                })
                            }
                            </tbody>
                        </table>
                    </>
                )
            }
        }
        return (
            <div id="my-account" className="col-xs-12 col-md-6">
                {title()}
                {chartElements()}
                {domElements()}
            </div>
        )
    }
}

MyAccountComponent.propTypes = {
    account: PropTypes.object.isRequired,
    money: PropTypes.object.isRequired
};

const mapMyAccountComponentStateToProps = (state, ownProps) => {
    return {
        account: state.account,
        money: state.money
    };
};

export default connect(
    mapMyAccountComponentStateToProps
)(MyAccountComponent);