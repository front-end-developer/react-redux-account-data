/**
 * Created by Mark Webley on 29/05/2019.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import './send-money.component.scss';
import '../../common/ui/common/styles/component.styles.scss';
import {ValidateSendMoney} from '../../common/ui/common/models/ValidateSendMoney';
import {SendMoneyFormErrors} from '../../common/ui/common/models/SendMoneyFormErrors';
import {DataUtil} from '../../common/ui/common/util/DataUtil';
import * as moneyActions from '../../../redux/actions/moneyAction';
import * as accountActions from '../../../redux/actions/accountAction';
import {EmailInputTemplate} from '../../common/ui/email/email-input.template';
import {TextInputTemplate} from '../../common/ui/input/text-input.template';
import MoneyTransferService from "../../service/MoneyTransfer.service";


class SendMoneyComponent extends Component {

    errorInForm = true;
    validationMessages = new ValidateSendMoney().messages;
    sendMoneyFormErrors = new SendMoneyFormErrors().formErrors;

    dataUtils = new DataUtil();
    state = {
        account: {
            name: '',
            email: '',
            amount: ''
        },
        money: {
            balance: 15000,
            withdrawal: 0
        },
        formErrors: this.sendMoneyFormErrors
    }

    componentDidMount() {
        this.props.accountActions(this.state.account);
        this.props.moneyActions(this.state.money);
    }

    insufficiantBalance() {
        if (this.state.money.balance - this.state.account.amount < 0) {
            // TODO: show modal
            console.log('INSUFICIENT BALANCE IN YOUR ACCOUNT');
            return true;
        }
        return false;
    }

    validateForm(group) {
        this.errorInForm = false;
        group.querySelectorAll('input').forEach((key) => {

            if (key.type === 'submit') {
                return;
            }

            let errorInKey;
            const control = this.validationMessages[key.name];
            switch (control.type) {
                case 'text':
                    errorInKey = {
                        key: key,
                        errors: !this.dataUtils.validateText(key.value),
                        errorMessage: control.minlength
                    };
                    break;
                case 'email':
                    errorInKey = {
                        key: key,
                        errors: !this.dataUtils.validateEmail(key.value),
                        errorMessage: control.minlength
                    };
                    break;
                case 'number':
                    errorInKey = {
                        key: key,
                        errors: !this.dataUtils.validateDigits(key.value),
                        errorMessage: control.minlength
                    };
                    break;
            }
            this.setState(prevState => ({
                formErrors: {
                    ...prevState.formErrors,
                    [key.name]: ''
                }
            }));

            if (errorInKey.errors) {
                const messages = this.validationMessages[key.name];
                errorInKey.key.classList.add('error');
                this.setState(prevState => ({
                    formErrors: {
                        ...prevState.formErrors,
                        [key.name]: errorInKey.errorMessage
                    }
                }));
                this.errorInForm = true;
            } else {
                errorInKey.key.classList.remove('error');
            }
        });
    }

    onSubmit = (event) => {
        event.preventDefault();
        event.stopPropagation();

        //TODO: fix this
        if ( this.insufficiantBalance() ) {
            // return false;
        }
        this.validateForm(event.target);
        if (!this.errorInForm) {
            this.sendMoneyTransfer();
            setTimeout(() => {
                this.props.accountActions(this.state.account);
                this.props.moneyActions(this.state.money);
            }, 500);

            // TODO: finish testing this restFul service API - Backend service
            // Fix so it send an object not an array
            /*
            MoneyTransferService.sendAccount(this.state.account).subscribe({
                next: result => console.log('observable send money transfer results: ', result),
                complete: () => console.log('done')
            });
            */
        }
    }

    sendMoneyTransfer() {
        if (this.insufficiantBalance()) {
            return false;
        }
        this.setState((prevState, props) => {
            return ({
                    money: {
                        ...prevState.money,
                        'balance':    (prevState.money.balance - parseFloat(this.state.account.amount)),
                        'withdrawal': (prevState.money.withdrawal + parseFloat(this.state.account.amount))
                    }
                }
            )
        });
    }

    inputHandler = (event) => {
        this.validateForm(event.target);
        if (!this.errorInForm || !this.insufficiantBalance()) {
            const account = {
                ...this.state.account,
                [event.target.name]: event.target.value
            }

            this.setState((prevState, props) => {
                return ({
                        account: account,
                        money: {
                            ...this.state.money
                        }
                    }
                )
            });
        }
    }

    render() {
        const title = () => {
            return (
                <h1 className="container">Send money</h1>
            )
        }
        const domElements = () => {
            return (
                <>
                    <section>
                        <form name="form-send-money" onSubmit={this.onSubmit} noValidate>
                            <fieldset>
                                <TextInputTemplate value={this.state.account.name}
                                                   formErrors={this.state.formErrors.name}
                                                   formErrorsString={this.state.formErrors.name}
                                                   fieldId="name" name="Name"
                                                   inputHandler={this.inputHandler} />
                            </fieldset>

                            <fieldset>
                                <EmailInputTemplate value={this.state.account.email}
                                                   formErrors={this.state.formErrors.name}
                                                   formErrorsString={this.state.formErrors.email}
                                                   fieldId="email" name="Email Address"
                                                   inputHandler={this.inputHandler} />
                            </fieldset>

                            <fieldset>
                                <TextInputTemplate value={this.state.account.amount}
                                                   formErrors={this.state.formErrors.name}
                                                   formErrorsString={this.state.formErrors.amount}
                                                   fieldId="amount" name="Amount"
                                                   inputHandler={this.inputHandler} />
                            </fieldset>

                            <input type="submit" className="btn btn-primary" value="Send" />
                        </form>
                    </section>
                </>
            )
        }
        return (
            <div id="send-money" className="col-xs-12 col-md-6">
                {title()}
                {domElements()}
            </div>
        )
    }
}

SendMoneyComponent.propTypes = {
    account: PropTypes.object.isRequired,
    accountActions: PropTypes.func.isRequired,
    moneyActions: PropTypes.func.isRequired
};

const mapSendMoneyComponentStateToProps = (state, ownProps) => {
    return {
        account: state.account
    };
};

const mapDispatchToSendMoneyComponentToProps = (dispatch) => {
    return {
        accountActions: account => dispatch(accountActions.accountActionCreator(account)),
        moneyActions: money => dispatch(moneyActions.sendMoneyToAccountActionCreator(money))
    };
};

export default connect(
    mapSendMoneyComponentStateToProps,
    mapDispatchToSendMoneyComponentToProps
)(SendMoneyComponent);
