import {of} from "rxjs/index";
import {catchError, switchMap} from "rxjs/operators";
import {fromFetch} from "rxjs/fetch";
import {APISERVICE} from "./APIS.service";

/**
 * Created by Mark Webley on 30/05/2019.
 */

export default class BankAccountService {

    /*
        NOTE: EXPERIMENTAL : INSTEAD OF ASYNC AWAIT OR AXIOS.

        GET BANK BALANCE:
            GET
                http://www.wisdomor.co.uk/exercises_1/account/index.php?balance=information
    */
    public static getMyAccount() {
        console.log('get bank account from the backend');
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        };
        return fromFetch(`${APISERVICE.API.GET_BANK_ACCOUNT}?balance=information`).pipe(
            switchMap(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    return of({ error: true, message: `Error ${response.status}` });
                }
            }),
            catchError(err => {
                console.error(err);
                return of({ error: true, message: err.message })
            })
        );
    }
}