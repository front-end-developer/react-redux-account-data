/**
 * Created by Mark Webley on 31/05/2019.
 */

export class APISERVICE {
    public static API: any = {
        POST_MONEY_TRANSFERS: 'http://www.wisdomor.co.uk/exercises_1/moneytransfer/index.php',
        GET_MONEY_TRANSFERS: 'http://www.wisdomor.co.uk/exercises_1/moneytransfer/index.php',
        GET_BANK_ACCOUNT: 'http://www.wisdomor.co.uk/exercises_1/account/index.php'
    }
}