/**
 * Created by Mark Webley on 30/05/2019.
 */
import {Observable} from "rxjs";
//import {map, flatMap, catchError, tap} from 'rxjs/operators';

import { of } from 'rxjs';
import { fromFetch } from 'rxjs/fetch';
import { switchMap, catchError } from 'rxjs/operators';
import {APISERVICE} from "./APIS.service";


export default class MoneyTransferService {

    /*
        NOTE: EXPERIMENTAL : INSTEAD OF ASYNC AWAIT OR AXIOS.

        POST
        http://www.wisdomor.co.uk/exercises_1/moneytransfer/index.php
            POST:
        name: 'Mark'
        email: ll@lll.com
        amount: 10

        success returns:
            {"result":"success","uuid":"14"}

        failor returns:
            {'error': 'db not updated'}
    */
    public static sendAccount(account: any) {
        console.log('sending money transfer to backend: ', JSON.stringify(account));
        const options = {
            method: 'POST',
            // mode: 'no-cors' <-- should work according to spec but it is not
            /*RequestInit: {
                mode: 'no-cors'
            }, */
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(account)
        };
        return fromFetch(APISERVICE.API.POST_MONEY_TRANSFERS, options).pipe(
            switchMap(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    return of({ error: true, message: `Error ${response.status}` });
                }
            }),
            catchError(err => {
                // Network or other error, handle appropriately
                console.error(err);
                return of({ error: true, message: err.message })
            })
        );
    }

    /*  NOTE: EXPERIMENTAL : INSTEAD OF ASYNC AWAIT OR AXIOS.

        GET ALL MONIES TRANSFERED:
            GET
        http://www.wisdomor.co.uk/exercises_1/moneytransfer/index.php?accounts=information
            POST:
        accounts: 'information'

        success returns:
        [
                {
                    name: "Mark Webley",
                    email: "test@wisdomor.co.uk",
                    amount: "11000",
                    date: null
                },
                {
                    name: "Mw",
                    email: "ll@ll.com",
                    amount: "123",
                    date: "2019-05-30 16:22:52"
                }
         ]
    */
    public static getAccounts() {
        return fromFetch(`${APISERVICE.API.GET_MONEY_TRANSFERS}?accounts=information`).pipe(
            switchMap(response => {
                if (response.ok) {
                    // OK return data
                    return response.json();
                } else {
                    // Server is returning a status requiring the client to try something else.
                    return of({ error: true, message: `Error ${response.status}` });
                }
            }),
            catchError(err => {
                // Network or other error, handle appropriately
                console.error(err);
                return of({ error: true, message: err.message })
            })
        );
    }
}