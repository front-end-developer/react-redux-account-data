/**
 * Created by Mark Webley on 29/05/2019.
 */
import React, {Component} from 'react';
import SendMoneyComponent from '../../modules/user-accounts/send-money/send-money.component';
import MyAccountComponent from '../../modules/user-accounts/my-accounts/my-account.component';

export default class AppContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <section className="container">
                <div className="row">
                    <SendMoneyComponent />
                    <MyAccountComponent />
                </div>
            </section>
        );
    }
}