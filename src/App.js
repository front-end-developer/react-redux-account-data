import React from 'react';
//import './App.scss';
import AppContainer from './modules/app-container/app.container';

function App() {
  return (
    <div className="App">
      <AppContainer />
    </div>
  );
}

export default App;
